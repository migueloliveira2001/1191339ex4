﻿Algoritmo NPositivo

ED: n,I,H, Npos, Nneg, hneg, Soma : inteiro;
Per, Media, mediaNeg :Real;

inicio

Soma<--0

Npos<--0

Nneg<--0

hneg<--0

escrever("Insira quant numeros: ")

ler(N)

Enquanto N < 0 Fazer

Escrever("Digite Novo numero")

Ler(N)

FimEnquanto

Para I <-- 1 até N passo 1 Fazer

Escrever ("Digite um Numero:")

Ler(H)

se H > 0 entao

Npos<--Npos+1

senão

hneg<-- hneg + H
Nneg<--Nneg + 1

FimSe

Soma<--Soma+H

FimPara

mediaNeg<--hneg/ N
 
Per<--(Npos/N)*100

Media<--Soma/N

Escrever("Percentagem:",Per,"%")

Escrever("Soma:",Soma)

Escrever("Media:",Media)

Escrever ("Soma dos números negativos: ", hneg)

Escrever ("Média negativos: ", mediaNeg)

final